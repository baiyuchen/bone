<?php
namespace app\bond\model;

use think\Model;
use think\Db;

class Coupon extends Model
{
    protected $name = 'bond_coupon';

    protected $autoWriteTimestamp = true;

    public static function getList($where = [])
    {
        return self::where($where)
            ->order('id desc')
            ->paginate()->each(function ($item, $key) {
                return $item;
            });
    }
}