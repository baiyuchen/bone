<?php
namespace app\bond\model;

use think\Model;
use think\Db;

class Specs extends Model
{
    protected $name = 'store_specs';

    protected $autoWriteTimestamp = true;

    public static function getList($where = [])
    {
        return self::where($where)
            ->order('id desc')
            ->paginate()->each(function ($item, $key) {
                return $item;
            });
    }
}