<?php
namespace app\bond\admin;

use think\Db;
use app\admin\controller\Admin;
use app\common\builder\ZBuilder;
use app\bond\model\Agent as AgentModel;

class Agent extends Admin
{
	public function index(){
		$map = $this->getMap();
        $list = AgentModel::getList($map);
        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setPageTitle('商品管理') // 设置页面标题
            ->setTableName('bond_agent') // 设置数据表名
            ->setSearch(['name' => '申请人姓名']) // 设置搜索参数
            ->addTimeFilter('create_time')
            ->addColumns([ // 批量添加列
                ['business', '加盟商名称'],
                ['name', '申请人姓名'],
                ['phone', '申请人电话'],
                ['address', '所在地址'],
                ['create_time', '入驻时间', 'datetime'],
                ['state', '审核状态', 'status','',[1=>'正常',2=>'异常']],
                ['right_button', '操作', 'btn']
            ])
            ->addTopButtons('add,delete') // 批量添加顶部按钮
            ->addRightButtons('edit,delete') // 批量添加右侧按钮
            ->setRowList($list) // 设置表格数据
            ->fetch(); // 渲染页面
    }

    //详情
    public function edit($id=''){
    	 if ($this->request->isPost()) {
            $data = $this->request->post();
            if (Db::name('bond_agent')->where('id',$id)->update($data)) {
                $this->success('更新成功','index');
            }
            $this->error('更新失败');
        }
        $info = AgentModel::get($id);
        // 使用ZBuilder快速创建表单
        return ZBuilder::make('form')
            ->setPageTitle('详情')// 设置页面标题
            ->addFormItems([ // 批量添加表单项
                ['static','business', '加盟商名称'],
            	['static','name', '申请人姓名'],
                ['static', 'phone', '申请人电话'],
            	['static', 'address', '所在地址'],
                ['radio', 'state', '状态','',[1=>'正常',2=>'异常']],
            ])
            ->setFormData($info)// 设置表单数据
            ->fetch();
    }

	
}