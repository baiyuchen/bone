<?php
namespace app\bond\admin;

use think\Db;
use app\admin\controller\Admin;
use app\common\builder\ZBuilder;
use app\bond\model\Coupon as CouponModel;

class Coupon extends Admin
{
	public function index(){
		$map = $this->getMap();
        $list = CouponModel::getList($map);
        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setPageTitle('优惠券列表') // 设置页面标题
            ->setTableName('bond_coupon') // 设置数据表名
            ->addTimeFilter('createtime')
            ->addColumns([ // 批量添加列
                ['total', '满XXX'],
                ['price', '减XXX'],
                ['image', '图片','picture'],
                ['state', '类型', 'status','',[1=>'全部商家可用',2=>'指定商家可用']],
                ['createtime', '添加时间', 'datetime'],
                ['right_button', '操作', 'btn']
            ])
            ->addTopButtons('add,delete') // 批量添加顶部按钮
            ->addRightButtons('edit,delete') // 批量添加右侧按钮
            ->setRowList($list) // 设置表格数据
            ->fetch(); // 渲染页面
    }

    //添加
    public function add(){
    	if ($this->request->isPost()) {
            $data = $this->request->post();
            $data['createtime'] = time();
            if (Db::name('bond_coupon')->insert($data)) {
                $this->success('更新成功','index');
            }
            $this->error('更新失败');
        }
        $agent = Db::name('bond_agent')->column('business','id');
        return ZBuilder::make('form')
            ->setPageTitle('添加优惠券')// 设置页面标题
            ->addFormItems([
                ['text','total', '满XX'],
                ['text','price', '减XXX'],
                ['image','image', '图片'],
                ['radio', 'state', '类型','',[1=>'全部商家可用',2=>'指定商家可用'],'1'],
                ['select', 'a_id', '店铺','',$agent],
            ])
            ->setTrigger('state', '2', 'a_id')
            ->fetch();
    }

    //用户详情
    public function edit($id=''){
    	 if ($this->request->isPost()) {
            $data = $this->request->post();
            if (Db::name('bond_coupon')->where('id',$id)->update($data)) {
                $this->success('更新成功','index');
            }
            $this->error('更新失败');
        }
        $info = CouponModel::get($id);
        $agent = Db::name('bond_agent')->column('business','id');
        // 使用ZBuilder快速创建表单
        return ZBuilder::make('form')
            ->setPageTitle('优惠券详情')// 设置页面标题
            ->addFormItems([ // 批量添加表单项
            	['text','total', '满XX'],
                ['text','price', '减XXX'],
                ['image','image', '图片'],
                ['radio', 'state', '类型','',[1=>'全部商家可用',2=>'指定商家可用'],'1'],
                ['select', 'a_id', '店铺','',$agent],
            ])
            ->setTrigger('state', '2', 'a_id')
            ->setFormData($info)// 设置表单数据
            ->fetch();
    }
}