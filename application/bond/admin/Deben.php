<?php
namespace app\bond\admin;

use think\Db;
use app\admin\controller\Admin;
use app\common\builder\ZBuilder;
use app\bond\model\Deben as DebenModel;

class Deben extends Admin
{
	public function index(){
		$map = $this->getMap();
        $list = DebenModel::getList($map);
        // $user = Db::name('bond_user')->column('nickname','id');
        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setPageTitle('订单列表') // 设置页面标题
            ->setTableName('bond_deben') // 设置数据表名
            ->addTimeFilter('create_time')
            ->addColumns([ // 批量添加列
                ['debt_main', '债权人'],
                ['debt_second', '债务人'],
                ['phone', '电话号码'],
                ['id_card_no', '身份证号码'],
                ['money', '转让金额'],
                ['text', '文字描述'],
                ['image', '图片凭证', 'picture'],
                ['date', '时间/天'],
                ['state', '状态', 'status','',[1=>'未卖出',2=>'已卖出']],
                ['create_time', '发布时间', 'datetime'],
                ['right_button', '操作', 'btn']
            ])
            ->addTopButtons('add,delete') // 批量添加顶部按钮
            ->addRightButtons('edit,delete') // 批量添加右侧按钮
            ->setRowList($list) // 设置表格数据
            ->fetch(); // 渲染页面
    }

    //添加
    public function add(){
    	if ($this->request->isPost()) {
            $data = $this->request->post();
            $data['create_time'] = time();
            $data['encryption'] = generate_rand_str();
            if (Db::name('bond_deben')->insert($data)) {
                $this->success('更新成功','index');
            }
            $this->error('更新失败');
        }

        return ZBuilder::make('form')
            ->setPageTitle('添加订单')// 设置页面标题
            ->addFormItems([
                ['text','debt_main', '债权人'],
                ['text','debt_second', '债务人'],
                ['text','phone', '电话号码'],
                ['text','id_card_no', '身份证号码'],
                ['text','money', '转让金额'],
                ['text','text', '文字描述'],
                ['image','image', '图片凭证'],
                ['text','date', '时间/天'],
                ['radio','state','状态','',[1=>'未卖出',2=>'已卖出'],'1'],
            ])
            ->fetch();
    }

    //用户详情
    public function edit($id=''){
    	 if ($this->request->isPost()) {
            $data = $this->request->post();
            if (Db::name('bond_Deben')->where('id',$id)->update($data)) {
                $this->success('更新成功','index');
            }
            $this->error('更新失败');
        }
        $info = DebenModel::get($id);
        // $user = Db::name('bond_user')->column('nickname','id');
        // 使用ZBuilder快速创建表单
        return ZBuilder::make('form')
            ->setPageTitle('订单详情')// 设置页面标题
            ->addFormItems([ // 批量添加表单项
            	['static','debt_main', '债权人'],
                ['static','debt_second', '债务人'],
                ['static','phone', '电话号码'],
                ['static','id_card_no', '身份证号码'],
                ['static','money', '转让金额'],
                ['static','text', '文字描述'],
                ['image','image', '图片凭证'],
                ['static','date', '时间/天'],
                ['radio','state','状态','',[1=>'未卖出',2=>'已卖出'],'1'],
            ])
            ->setFormData($info)// 设置表单数据
            ->fetch();
    }
}