<?php
namespace app\bond\admin;

use think\Db;
use app\admin\controller\Admin;
use app\common\builder\ZBuilder;
use app\bond\model\Clas as ClasModel;

class Clas extends Admin
{
	public function index(){
		$map = $this->getMap();
        $list = ClasModel::getList($map);
        // $type = Db::name('store_type')->column('name','id');
        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setPageTitle('商品管理') // 设置页面标题
            ->setTableName('store_class') // 设置数据表名
            ->setSearch(['name' => '商品名称']) // 设置搜索参数
            // ->addTimeFilter('createtime')
            ->addColumns([ // 批量添加列
                ['class_name', '商品名称'],
                ['create_time', '添加时间', 'datetime'],
                ['right_button', '操作', 'btn']
            ])
            ->addTopButtons('add,delete') // 批量添加顶部按钮
            ->addRightButtons('edit,delete') // 批量添加右侧按钮
            ->setRowList($list) // 设置表格数据
            ->fetch(); // 渲染页面
    }

    //详情
    public function edit($id=''){
    	 if ($this->request->isPost()) {
            $data = $this->request->post();
            if (Db::name('store_class')->where('id',$id)->update($data)) {
                $this->success('更新成功','index');
            }
            $this->error('更新失败');
        }
        $info = ClasModel::get($id);
        // $type = Db::name('store_type')->column('name','id');
        // 使用ZBuilder快速创建表单
        return ZBuilder::make('form')
            ->setPageTitle('商品详情')// 设置页面标题
            ->addFormItems([ // 批量添加表单项
            	['text','class_name', '商品名称'],
            ])
            ->setFormData($info)// 设置表单数据
            ->fetch();
    }

    //添加
    public function add(){
    	if ($this->request->isPost()) {
            $data = $this->request->post();
            $data['create_time'] = time();
            if (Db::name('store_class')->insert($data)) {
                $this->success('更新成功','index');
            }
            $this->error('更新失败');
        }
        // $type = Db::name('store_type')->column('name','id');
        return ZBuilder::make('form')
            ->setPageTitle('添加用户')// 设置页面标题
            ->addFormItems([
                ['text','class_name', '商品名称'],
            ])
            ->fetch();
    }
	
}