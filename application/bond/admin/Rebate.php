<?php
namespace app\bond\admin;

use think\Db;
use app\admin\controller\Admin;
use app\common\builder\ZBuilder;
use app\bond\model\Rebate as RebateModel;

class Rebate extends Admin
{
	public function index(){
        if ($this->request->isPost()) {
            $data = $this->request->post();
            if (Db::name('bond_rebate')->where('id',1)->update($data)) {
                $this->success('更新成功','index');
            }
            $this->error('更新失败');
        }
        $info = RebateModel::get(1);
        // 使用ZBuilder快速创建表单
        return ZBuilder::make('form')
            ->setPageTitle('分销设置')// 设置页面标题
            ->addFormItems([ // 批量添加表单项
                ['radio','state', '分销类型','',[1=>'一级分销',2=>'二级分销',3=>'三级分销'],'1'],
                ['radio','status', '分销类别','',[1=>'按利率',2=>'按返利'],'1'],
                ['text', 'interest_one', '一级利率'],
                ['text', 'interest_two', '二级利率'],
                ['text', 'interest_three', '三级利率'],
                ['text', 'benefit_one', '一级返利'],
                ['text', 'benefit_two', '二级返利'],
                ['text', 'benefit_three', '三级返利'],
            ])
            ->hideBtn('back')
            ->setTrigger('status', '1', 'interest_one,interest_two,interest_three')
            ->setTrigger('status', '2', 'benefit_one,benefit_two,benefit_three')
            ->setFormData($info)// 设置表单数据
            ->fetch();
    }
}