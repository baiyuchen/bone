<?php
namespace app\bond\admin;

use think\Db;
use app\admin\controller\Admin;
use app\common\builder\ZBuilder;
use app\bond\model\User as UserModel;

class User extends Admin
{
	public function index(){
		$map = $this->getMap();
        $list = UserModel::getList($map);

        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setPageTitle('用户管理') // 设置页面标题
            ->setTableName('bond_user') // 设置数据表名
            ->setSearch(['nickname' => '用户昵称']) // 设置搜索参数
            ->addTimeFilter('create_time')
            ->addColumns([ // 批量添加列
                // ['id', 'id号'],
            	['nickname', '用户昵称'],
            	['image', '用户头像', 'picture'],
                ['phone', '用户电话'],
                ['sex', '性别', 'status','',[1=>'男',2=>'女',0=>'未知']],
                ['state', '身份', 'status','',[1=>'债务人',2=>'债权人']],
                ['create_time', '注册时间', 'datetime'],
                ['right_button', '操作', 'btn']
            ])
            ->addTopButtons('add,delete') // 批量添加顶部按钮
            ->addRightButtons('edit,delete') // 批量添加右侧按钮
            ->setRowList($list) // 设置表格数据
            ->fetch(); // 渲染页面
    }

    //用户详情
    public function edit($id=''){
    	 if ($this->request->isPost()) {
            $data = $this->request->post();
            if (Db::name('bond_user')->where('id',$id)->update($data)) {
                $this->success('更新成功','index');
            }
            $this->error('更新失败');
        }
        $info = UserModel::get($id);
        // 使用ZBuilder快速创建表单
        return ZBuilder::make('form')
            ->setPageTitle('用户详情')// 设置页面标题
            ->addFormItems([ // 批量添加表单项
            	['text','nickname', '用户昵称'],
            	['image', 'image', '用户头像'],
                ['text', 'phone', '用户电话'],
                ['radio', 'sex', '性别','',[1=>'男',2=>'女']],
                ['radio', 'state', '用户身份','',[1=>'债务人',2=>'债权人'],'1'],
            ])
            ->setFormData($info)// 设置表单数据
            ->fetch();
    }

    //添加
    public function add(){
    	if ($this->request->isPost()) {
            $data = $this->request->post();
            $data['create_time'] = time();
            // $data['encryption'] = generate_rand_str();
            
            if (Db::name('bond_user')->insert($data)) {
                $this->success('更新成功','index');
            }
            $this->error('更新失败');
        }

        return ZBuilder::make('form')
            ->setPageTitle('添加用户')// 设置页面标题
            ->addFormItems([
                ['text', 'nickname', '用户昵称'],
                ['image', 'image', '用户头像'],
                ['text', 'phone', '用户电话'],
                ['radio', 'sex', '性别','',[1=>'男',2=>'女']],
                ['radio', 'state', '用户身份','',[1=>'债务人',2=>'债权人'],'1'],
            ])
            ->fetch();
    }
	
}