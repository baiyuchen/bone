<?php
namespace app\bond\admin;

use think\Db;
use app\admin\controller\Admin;
use app\common\builder\ZBuilder;
use app\bond\model\Joinin as JoininModel;

class Joinin extends Admin
{
	public function index(){
		$map = $this->getMap();
        $list = JoininModel::getList($map);
        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setPageTitle('商品管理') // 设置页面标题
            ->setTableName('bond_joinin') // 设置数据表名
            ->setSearch(['name' => '申请人姓名']) // 设置搜索参数
            // ->addTimeFilter('createtime')
            ->addColumns([ // 批量添加列
                ['business', '加盟商名称'],
                ['name', '申请人姓名'],
                ['phone', '申请人电话'],
                ['address', '所在地址'],
                ['text', '申请原因'],
                ['create_time', '申请时间', 'datetime'],
                ['state', '审核状态', 'status','',[1=>'审核通过',2=>'审核失败',0=>'未审核']],
                ['right_button', '操作', 'btn']
            ])
            ->addTopButtons('add,delete') // 批量添加顶部按钮
            ->addRightButtons('edit,delete') // 批量添加右侧按钮
            ->setRowList($list) // 设置表格数据
            ->fetch(); // 渲染页面
    }

    //详情
    public function edit($id=''){
    	 if ($this->request->isPost()) {
            $data = $this->request->post();
            if($data['state']==1){
                $list = Db::name('bond_joinin')->where('id',$id)->field('name,phone,address,business')->find();
                $phone = $list['phone'];
                if(!Db::name('bond_agent')->where('phone',$phone)->find()){
                    $list['create_time'] = time();
                    Db::name('bond_agent')->insert($list);
                } 
            }
            if (Db::name('bond_joinin')->where('id',$id)->update($data)) {
                $this->success('更新成功','index');
            }
            $this->error('更新失败');
        }
        $info = JoininModel::get($id);
        // 使用ZBuilder快速创建表单
        return ZBuilder::make('form')
            ->setPageTitle('详情')// 设置页面标题
            ->addFormItems([ // 批量添加表单项
                ['static','business', '加盟商名称'],
            	['static','name', '申请人姓名'],
                ['static', 'phone', '申请人电话'],
            	['static', 'address', '所在地址'],
                ['static','text', '申请原因'],
                ['radio', 'state', '审核状态','',[1=>'审核通过',2=>'审核失败',0=>'未审核']],
            ])
            ->setFormData($info)// 设置表单数据
            ->fetch();
    }

	
}