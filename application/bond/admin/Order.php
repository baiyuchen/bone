<?php
namespace app\bond\admin;

use think\Db;
use app\admin\controller\Admin;
use app\common\builder\ZBuilder;
use app\bond\model\Order as OrderModel;

class Order extends Admin
{
	public function index(){
		$map = $this->getMap();
        $list = OrderModel::getList($map);
        $user = Db::name('bond_user')->column('nickname','id');
        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setPageTitle('订单列表') // 设置页面标题
            ->setTableName('store_order') // 设置数据表名
            ->setSearch(['order' => '订单号']) // 设置搜索参数
            ->addTimeFilter('create_time')
            ->addColumns([ // 批量添加列
                ['id', '序号'],
                ['order', '订单号'],
                ['good_name', '商品名称'],
                ['image', '商品图片', 'picture'],
                ['userid', '购买人','status','',$user],
                ['create_time', '下单时间', 'datetime'],
                ['specifications', '规格'],
                ['number', '数量'],
                ['price', '价格'],
                ['total', '总价'],
                ['status', '状态','status','',[0=>'待付款',1=>'已付款',2=>'已取消',3=>'已过期',4=>'已发货',5=>'已收货',6=>'发起售后',7=>'客服审核',8=>'审核通过',9=>'完成售后']],
                ['right_button', '操作', 'btn']
            ])
            ->addTopButtons('delete') // 批量添加顶部按钮
            ->addRightButtons('edit,delete') // 批量添加右侧按钮
            ->setRowList($list) // 设置表格数据
            ->fetch(); // 渲染页面
    }

    // //添加
    // public function add(){
    // 	if ($this->request->isPost()) {
    //         $data = $this->request->post();
    //         $data['create_time'] = time();
    //         $data['encryption'] = generate_rand_str();
    //         if (Db::name('store_order')->insert($data)) {
    //             $this->success('更新成功','index');
    //         }
    //         $this->error('更新失败');
    //     }

    //     return ZBuilder::make('form')
    //         ->setPageTitle('添加订单')// 设置页面标题
    //         ->addFormItems([
    //             ['text','order', '订单号'],
    //             ['text','good_name', '商品名称'],
    //             ['image', 'image', '商品图片'],
    //             // ['text','business', '店铺名称'],
    //             ['text','userid', '购买人'],
    //             ['text','specifications', '规格'],
    //             ['text','number', '数量'],
    //             ['text','price', '价格'],
    //             ['text','total', '总价'],
    //             ['radio', 'status', '状态','',[0=>'待付款',1=>'已付款',2=>'已取消',3=>'已过期'],'0'],
    //             ['radio','state','是否售后','',[0=>'否',1=>'是'],'0'],
    //             ['radio','sale','售后状态','',[1=>'发起售后',2=>'客服审核',3=>'审核通过',4=>'完成'],'1'],
    //         ])
    //         ->fetch();
    // }

    //用户详情
    public function edit($id=''){
    	 if ($this->request->isPost()) {
            $data = $this->request->post();
            if (Db::name('store_order')->where('id',$id)->update($data)) {
                $this->success('更新成功','index');
            }
            $this->error('更新失败');
        }
        $info = OrderModel::get($id);
        $user = Db::name('bond_user')->column('nickname','id');
        // 使用ZBuilder快速创建表单
        return ZBuilder::make('form')
            ->setPageTitle('订单详情')// 设置页面标题
            ->addFormItems([ // 批量添加表单项
            	['static','order', '订单号'],
                ['static','good_name', '商品名称'],
                ['image', 'image', '商品图片'],
                ['select','userid', '购买人','',$user],
                ['static','specifications', '规格'],
                ['static','number', '数量'],
                ['static','price', '价格'],
                ['static','total', '总价'],
                ['radio', 'status', '状态','',[0=>'待付款',1=>'已付款',2=>'已取消',3=>'已过期',4=>'已发货',5=>'已收货',6=>'发起售后',7=>'客服审核',8=>'审核通过',9=>'完成售后'],'0'],
            ])
            ->setFormData($info)// 设置表单数据
            ->fetch();
    }
}