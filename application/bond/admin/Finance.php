<?php
namespace app\bond\admin;

use think\Db;
use app\admin\controller\Admin;
use app\common\builder\ZBuilder;
use app\bond\model\Finance as FinanceModel;

class Finance extends Admin
{   
    //充值
	public function recharge(){
		$map = $this->getMap();
        $list = FinanceModel::getList(['state'=>1]);
        $user = Db::name('bond_user')->column('nickname','id');
        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setPageTitle('商品管理') // 设置页面标题
            ->setTableName('finance') // 设置数据表名
            // ->setSearch(['name' => '商品名称']) // 设置搜索参数
            ->addTimeFilter('create_time')
            ->addColumns([ // 批量添加列
                ['title', '标题'],
                ['value', '值'],
                ['uid', '用户','status','',$user],
                ['create_time', '添加时间', 'datetime'],
            ])
            ->setRowList($list) // 设置表格数据
            ->fetch(); // 渲染页面
    }

    //消费
    public function consume(){
        $map = $this->getMap();
        $list = FinanceModel::getList(['state'=>2]);
        $user = Db::name('bond_user')->column('nickname','id');
        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setPageTitle('商品管理') // 设置页面标题
            ->setTableName('finance') // 设置数据表名
            // ->setSearch(['name' => '商品名称']) // 设置搜索参数
            ->addTimeFilter('create_time')
            ->addColumns([ // 批量添加列
               ['title', '标题'],
                ['value', '值'],
                ['uid', '用户','status','',$user],
                ['create_time', '添加时间', 'datetime'],
            ])
            ->setRowList($list) // 设置表格数据
            ->fetch(); // 渲染页面
    }

    
	
}