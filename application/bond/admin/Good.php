<?php
namespace app\bond\admin;

use think\Db;
use app\admin\controller\Admin;
use app\common\builder\ZBuilder;
use app\bond\model\Good as GoodModel;

class Good extends Admin
{
	public function index(){
		$map = $this->getMap();
        $list = GoodModel::getList($map);
        $type = Db::name('store_class')->column('class_name','id');
        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setPageTitle('商品管理') // 设置页面标题
            ->setTableName('store_goods') // 设置数据表名
            ->setSearch(['name' => '商品名称']) // 设置搜索参数
            // ->addTimeFilter('createtime')
            ->addColumns([ // 批量添加列
                ['good_name', '商品名称'],
                ['good_cover', '商品封面', 'picture'],
                ['good_image', '商品图片', 'picture'],
                ['good_price', '价格'],
                ['good_detail', '商品详情'],
                ['class_id', '类型','status','',$type],
                ['specifications', '规格/加单位'],
                ['good_stock', '库存'],
                ['state', '状态', 'status','',[1=>'上架',2=>'下架'],'1'],
                ['status', '审核状态', 'status','',[1=>'审核通过',2=>'审核失败',0=>'未审核']],
                ['right_button', '操作', 'btn']
            ])
            ->addTopButtons('add,delete') // 批量添加顶部按钮
            ->addRightButtons('edit,delete') // 批量添加右侧按钮
            ->setRowList($list) // 设置表格数据
            ->fetch(); // 渲染页面
    }

    //详情
    public function edit($id=''){
    	 if ($this->request->isPost()) {
            $data = $this->request->post();
            if (Db::name('store_goods')->where('id',$id)->update($data)) {
                $this->success('更新成功','index');
            }
            $this->error('更新失败');
        }
        $info = GoodModel::get($id);
        $type = Db::name('store_class')->column('class_name','id');
        // 使用ZBuilder快速创建表单
        return ZBuilder::make('form')
            ->setPageTitle('商品详情')// 设置页面标题
            ->addFormItems([ // 批量添加表单项
            	['text','good_name', '商品名称'],
                ['image', 'good_cover', '商品封面'],
            	['image', 'good_image', '商品图片'],
                ['text','good_price', '价格'],
                ['ueditor', 'good_detail', '商品详情'],
                ['select', 'class_id', '类型','',$type],
                ['text','specifications', '规格/加单位'],
                ['text','good_stock', '库存'],
                ['radio', 'state', '状态','',[1=>'上架',2=>'下架'],'1'],
                ['radio', 'status', '审核状态','',[1=>'审核通过',2=>'审核失败',0=>'未审核'],'1'],
            ])
            ->setFormData($info)// 设置表单数据
            ->fetch();
    }

    //添加
    public function add(){
    	if ($this->request->isPost()) {
            $data = $this->request->post();
            $data['create_time'] = time();
            if (Db::name('store_goods')->insert($data)) {
                $this->success('更新成功','index');
            }
            $this->error('更新失败');
        }
        $type = Db::name('store_class')->column('class_name','id');
        return ZBuilder::make('form')
            ->setPageTitle('添加用户')// 设置页面标题
            ->addFormItems([
                ['text','good_name', '商品名称'],
                ['image', 'good_cover', '商品封面'],
                ['image', 'good_image', '商品图片'],
                ['text','good_price', '价格'],
                ['ueditor', 'good_detail', '商品详情'],
                ['select', 'class_id', '类型','',$type],
                ['text','specifications', '规格/加单位'],
                ['text','good_stock', '库存'],
                ['radio', 'state', '状态','',[1=>'上架',2=>'下架'],'1'],
                ['radio', 'status', '审核状态','',[1=>'审核通过',2=>'审核失败',0=>'未审核']],
            ])
            ->fetch();
    }
	
}